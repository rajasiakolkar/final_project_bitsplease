/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author aayush
 */
public class University {
    private String userName;
    private String password;
    private String uniName;
    private String address;
    private String phNo;
    private int seats;
    private String uniId;
    private String uniEmail;
    private boolean s20=false;
    private boolean s50=false;
    private boolean s70=false;
    private boolean s100=false;
    private boolean loggedIn=false;
    private boolean approved=false;
    private String status;
    private List<Student> uniAcceptedStudents;
    private List<Student> uniSponsoredStudents;
    private List<Student> uniAttendingStudents;
    
    public University(){
        uniAcceptedStudents= new ArrayList<>();
        uniSponsoredStudents = new ArrayList<>();
        uniAttendingStudents = new ArrayList<>();
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getUniName() {
        return uniName;
    }

    public void setUniName(String uniName) {
        this.uniName = uniName;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getPhNo() {
        return phNo;
    }

    public void setPhNo(String phNo) {
        this.phNo = phNo;
    }

    public int getSeats() {
        return seats;
    }

    public void setSeats(int seats) {
        this.seats = seats;
    }

    public String getUniId() {
        return uniId;
    }

    public void setUniId(String uniId) {
        this.uniId = uniId;
    }

    public boolean isLoggedIn() {
        return loggedIn;
    }

    public void setLoggedIn(boolean loggedIn) {
        this.loggedIn = loggedIn;
    }

    public boolean isApproved() {
        return approved;
    }

    public void setApproved(boolean approved) {
        this.approved = approved;
    }

    public String getUniEmail() {
        return uniEmail;
    }

    public void setUniEmail(String uniEmail) {
        this.uniEmail = uniEmail;
    }

    public boolean isS20() {
        return s20;
    }

    public void setS20(boolean s20) {
        this.s20 = s20;
    }

    public boolean isS50() {
        return s50;
    }

    public void setS50(boolean s50) {
        this.s50 = s50;
    }

    public boolean isS70() {
        return s70;
    }

    public void setS70(boolean s70) {
        this.s70 = s70;
    }

    public boolean isS100() {
        return s100;
    }

    public void setS100(boolean s100) {
        this.s100 = s100;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public List<Student> getUniAcceptedStudents() {
        return uniAcceptedStudents;
    }

    public void setUniAcceptedStudents(List<Student> uniAcceptedStudents) {
        this.uniAcceptedStudents = uniAcceptedStudents;
    }

    public List<Student> getUniSponsoredStudents() {
        return uniSponsoredStudents;
    }

    public void setUniSponsoredStudents(List<Student> uniSponsoredStudents) {
        this.uniSponsoredStudents = uniSponsoredStudents;
    }

    public List<Student> getUniAttendingStudents() {
        return uniAttendingStudents;
    }

    public void setUniAttendingStudents(List<Student> uniAttendingStudents) {
        this.uniAttendingStudents = uniAttendingStudents;
    }
    
    
    
}
