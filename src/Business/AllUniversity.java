/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author aayush
 */
public class AllUniversity {
    private ArrayList<University> allUniversity;
    private ArrayList<University> pendingUniversity;

    public AllUniversity() {
        allUniversity = new ArrayList<>();
        pendingUniversity= new ArrayList<>();
    }
    
    public University addUniversity(){
        University u = new University();
        allUniversity.add(u);
        return u;
    }
    
    

    public ArrayList<University> getAllUniversity() {
        return allUniversity;
    }

    public void setAllUniversity(ArrayList<University> allUniversity) {
        this.allUniversity = allUniversity;
    }

    public ArrayList<University> getPendingUniversity() {
        return pendingUniversity;
    }

    public void setPendingUniversity(ArrayList<University> pendingUniversity) {
        this.pendingUniversity = pendingUniversity;
    }
    
    
}
