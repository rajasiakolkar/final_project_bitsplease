/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.Icon;

/**
 *
 * @author aayush
 */
public class Student {
    private String name;
    private double amount;
    private Icon img;
    private int minvalue;
    private String status;
    private String myEmailAdd;
    private String uName;
    private String pass;
    private String openUni;
    private String uni1;
    private String uni2;
    private List<String> sponsorEmail = new ArrayList<>();
    private boolean loggedin = false;
    HashMap<String , List<Integer>> sponsorToDonations = new HashMap<>();
    private int uni1Accepted=0;
    private int uni2Accepted=0;
    private boolean adminApproved=false;
    private boolean donationMade=false;
    private int sponsoredAmt=0;  //created after uni sponsored uni1
    private double moneySponUni=0;
    private double moneyRemUni=0;
    private int sponsoredAmt2=0;  //created after uni sponsored uni2
    private double moneySponUni2=0;
    private double moneyRemUni2=0;
    private boolean bankApproved=false;//for financial doc approval
    private String chosenBank;
    private boolean processCom=false;//after confirming uni

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getAmount() {
        return amount;
    }

    public void setAmount(double amount) {
        this.amount = amount;
    }

    public Icon getImg() {
        return img;
    }

    public void setImg(Icon img) {
        this.img = img;
    }

    public int getMinvalue() {
        return minvalue;
    }

    public void setMinvalue(int minvalue) {
        this.minvalue = minvalue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getMyEmailAdd() {
        return myEmailAdd;
    }

    public void setMyEmailAdd(String myEmailAdd) {
        this.myEmailAdd = myEmailAdd;
    }

    public String getuName() {
        return uName;
    }

    public void setuName(String uName) {
        this.uName = uName;
    }

    public String getPass() {
        return pass;
    }

    public void setPass(String pass) {
        this.pass = pass;
    }

    public boolean isLoggedin() {
        return loggedin;
    }

    public void setLoggedin(boolean loggedin) {
        this.loggedin = loggedin;
    }

    public List<String> getSponsorEmail() {
        return sponsorEmail;
    }

    public void setSponsorEmail(List<String> sponsorEmail) {
        this.sponsorEmail = sponsorEmail;
    }

    public HashMap<String, List<Integer>> getSponsorToDonations() {
        return sponsorToDonations;
    }

    public void setSponsorToDonations(HashMap<String, List<Integer>> sponsorToDonations) {
        this.sponsorToDonations = sponsorToDonations;
    }

    public String getUni1() {
        return uni1;
    }

    public void setUni1(String uni1) {
        this.uni1 = uni1;
    }

    public String getUni2() {
        return uni2;
    }

    public void setUni2(String uni2) {
        this.uni2 = uni2;
    }

    public String getOpenUni() {
        return openUni;
    }

    public void setOpenUni(String openUni) {
        this.openUni = openUni;
    }

    public int getUni1Accepted() {
        return uni1Accepted;
    }

    public void setUni1Accepted(int uni1Accepted) {
        this.uni1Accepted = uni1Accepted;
    }

    public int getUni2Accepted() {
        return uni2Accepted;
    }

    public void setUni2Accepted(int uni2Accepted) {
        this.uni2Accepted = uni2Accepted;
    }

    public boolean isAdminApproved() {
        return adminApproved;
    }

    public void setAdminApproved(boolean adminApproved) {
        this.adminApproved = adminApproved;
    }

    public int getSponsoredAmt() {
        return sponsoredAmt;
    }

    public void setSponsoredAmt(int sponsoredAmt) {
        this.sponsoredAmt = sponsoredAmt;
    }

    public double getMoneySponUni() {
        return moneySponUni;
    }

    public void setMoneySponUni(double moneySponUni) {
        this.moneySponUni = moneySponUni;
    }

    public double getMoneyRemUni() {
        return moneyRemUni;
    }

    public void setMoneyRemUni(double moneyRemUni) {
        this.moneyRemUni = moneyRemUni;
    }

    public boolean isBankApproved() {
        return bankApproved;
    }

    public void setBankApproved(boolean bankApproved) {
        this.bankApproved = bankApproved;
    }

    public String getChosenBank() {
        return chosenBank;
    }

    public void setChosenBank(String chosenBank) {
        this.chosenBank = chosenBank;
    }

    public int getSponsoredAmt2() {
        return sponsoredAmt2;
    }

    public void setSponsoredAmt2(int sponsoredAmt2) {
        this.sponsoredAmt2 = sponsoredAmt2;
    }

    public double getMoneySponUni2() {
        return moneySponUni2;
    }

    public void setMoneySponUni2(double moneySponUni2) {
        this.moneySponUni2 = moneySponUni2;
    }

    public double getMoneyRemUni2() {
        return moneyRemUni2;
    }

    public void setMoneyRemUni2(double moneyRemUni2) {
        this.moneyRemUni2 = moneyRemUni2;
    }

    public boolean isProcessCom() {
        return processCom;
    }

    public void setProcessCom(boolean processCom) {
        this.processCom = processCom;
    }

    public boolean isDonationMade() {
        return donationMade;
    }

    public void setDonationMade(boolean donationMade) {
        this.donationMade = donationMade;
    }
    
    

    
    @Override
    public String toString() {
        return name+amount+status+myEmailAdd+uName+openUni+uni1+uni2; //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
