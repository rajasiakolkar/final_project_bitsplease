/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Business;

import java.util.ArrayList;

/**
 *
 * @author aayush
 */
public class AllStudents {
    private ArrayList<Student> studentDir;
    private ArrayList<Student> waitStudentDir;
    private ArrayList<Student> studentRecordDir;
    private ArrayList<Student> waitingUniApproval;
    private ArrayList<Student> uniApproved;
    private ArrayList<Student> WaitingBankApproval;

    public AllStudents() {
        studentDir = new ArrayList<>();
        waitStudentDir =  new ArrayList<>();
        studentRecordDir = new ArrayList<>();
        waitingUniApproval= new ArrayList<>();
        uniApproved= new ArrayList<>();
        WaitingBankApproval= new ArrayList<>();
    }

    public ArrayList<Student> getStudentDir() {
        return studentDir;
    }

    public void setStudentDir(ArrayList<Student> studentDir) {
        this.studentDir = studentDir;
    }

    public ArrayList<Student> getWaitStudentDir() {
        return waitStudentDir;
    }

    public void setWaitStudentDir(ArrayList<Student> waitStudentDir) {
        this.waitStudentDir = waitStudentDir;
    }

    public ArrayList<Student> getStudentRecordDir() {
        return studentRecordDir;
    }

    public void setStudentRecordDir(ArrayList<Student> studentRecordDir) {
        this.studentRecordDir = studentRecordDir;
    }

    public ArrayList<Student> getWaitingUniApproval() {
        return waitingUniApproval;
    }

    public void setWaitingUniApproval(ArrayList<Student> waitingUniApproval) {
        this.waitingUniApproval = waitingUniApproval;
    }

    public ArrayList<Student> getUniApproved() {
        return uniApproved;
    }

    public void setUniApproved(ArrayList<Student> uniApproved) {
        this.uniApproved = uniApproved;
    }

    public ArrayList<Student> getWaitingBankApproval() {
        return WaitingBankApproval;
    }

    public void setWaitingBankApproval(ArrayList<Student> WaitingBankApproval) {
        this.WaitingBankApproval = WaitingBankApproval;
    }
    
    
    
    public Student addStudent(){
        Student s = new Student();
        studentDir.add(s);
        return s;
    }
    
    public Student addWaitStudent(){
        Student s = new Student();
        waitStudentDir.add(s);
        return s;
    }
    
    
}
