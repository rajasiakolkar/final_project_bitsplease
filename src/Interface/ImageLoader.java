/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface;

import java.net.URL;
import javax.swing.ImageIcon;

/**
 *
 * @author aayush
 */
public final class ImageLoader {
    
    public static final ImageIcon loadImageIcon( final String path ) {

        final URL res = ClassLoader.getSystemResource( path );

        return new ImageIcon( res );
    }
}
