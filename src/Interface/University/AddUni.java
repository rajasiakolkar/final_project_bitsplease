/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.University;


import Business.AllUniversity;
import Business.University;
import Business.db40.DB4OUniversity;
import Business.db40.Log;
import Interface.MainJFrame;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
 
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;


/**
 *
 * @author aayush
 */
public class AddUni extends javax.swing.JPanel {

    /**
     * Creates new form AddUni
     */
    final Browser browser = new Browser();
    BrowserView view = new BrowserView(browser);
    private JPanel CardSequenceJPanel;
    private University u;
    private MainJFrame main;
//    private DB4OUniversity dB4OUni = DB4OUniversity.getInstance();
    Log financialAid;
    private int seatsValid=0;
    public AddUni(JPanel CardSequenceJPanel,University u,MainJFrame main,Log financialAid) {
        initComponents();
        this.setVisible(true);
        searchTxtField.setEditable(false);
        addressTxtField.setEditable(false);
        phNoTxtField.setEditable(false);
        emailTxtField.setEditable(false);
        this.CardSequenceJPanel=CardSequenceJPanel;
        this.u=u;
        this.main = main;
        this.financialAid=financialAid;
        loadMap();
    }

    public void loadMap(){
        searchTxtField.setText(u.getUniName());
        String s = u.getUniName();
        s=s.replace(" ", "+");
        
        map.setSize(1800, 1000);
        
        browser.loadURL("https://www.google.com/maps/search/?api=1&query="+s);
        map.add(view);
    }
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        searchTxtField = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        map = new javax.swing.JPanel();
        addressTxtField = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        phNoTxtField = new javax.swing.JTextField();
        addAddressBtn = new javax.swing.JButton();
        addPhNobtn = new javax.swing.JButton();
        jLabel5 = new javax.swing.JLabel();
        seatsTxtField = new javax.swing.JTextField();
        updateUniBtn = new javax.swing.JButton();
        firstChkBox = new javax.swing.JCheckBox();
        secondChkBox = new javax.swing.JCheckBox();
        thirdChkBox = new javax.swing.JCheckBox();
        forthChkBox = new javax.swing.JCheckBox();
        jLabel6 = new javax.swing.JLabel();
        emailTxtField = new javax.swing.JTextField();
        emailAddBtn = new javax.swing.JButton();

        setBackground(new java.awt.Color(255, 255, 255));

        map.setBackground(new java.awt.Color(255, 255, 255));
        map.setLayout(new java.awt.BorderLayout());
        jScrollPane1.setViewportView(map);

        jLabel2.setText("University:");

        jLabel3.setText("Address:");

        jLabel4.setText("Phone number:");

        addAddressBtn.setText("Add");
        addAddressBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addAddressBtnActionPerformed(evt);
            }
        });

        addPhNobtn.setText("Add");
        addPhNobtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                addPhNobtnActionPerformed(evt);
            }
        });

        jLabel5.setText("Seats:");

        seatsTxtField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                seatsTxtFieldKeyReleased(evt);
            }
        });

        updateUniBtn.setBackground(new java.awt.Color(255, 255, 255));
        updateUniBtn.setText("Update Details");
        updateUniBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                updateUniBtnActionPerformed(evt);
            }
        });

        firstChkBox.setText("20% Sponsorship");

        secondChkBox.setText("50% Sponsorship");

        thirdChkBox.setText("70% Sponsorship");

        forthChkBox.setText("100% Sponsorship");

        jLabel6.setText("Email Add:");

        emailAddBtn.setText("Add");
        emailAddBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                emailAddBtnActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
            .addGroup(layout.createSequentialGroup()
                .addGap(48, 48, 48)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(jLabel6)
                        .addGap(33, 33, 33)
                        .addComponent(emailTxtField))
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel4)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(phNoTxtField))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(jLabel2)
                                .addComponent(jLabel3))
                            .addGap(32, 32, 32)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                .addComponent(addressTxtField)
                                .addComponent(searchTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 195, javax.swing.GroupLayout.PREFERRED_SIZE)))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(addAddressBtn)
                    .addComponent(addPhNobtn)
                    .addComponent(emailAddBtn))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 141, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(forthChkBox)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(seatsTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 96, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(secondChkBox)
                    .addComponent(firstChkBox)
                    .addComponent(thirdChkBox))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 137, Short.MAX_VALUE)
                .addComponent(updateUniBtn)
                .addGap(112, 112, 112))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 453, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(16, 16, 16)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(searchTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel2))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(addressTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel3)
                            .addComponent(addAddressBtn))
                        .addGap(13, 13, 13)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel4)
                            .addComponent(phNoTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(addPhNobtn))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel6)
                            .addComponent(emailTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emailAddBtn)))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(8, 8, 8)
                        .addComponent(firstChkBox)
                        .addGap(8, 8, 8)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(secondChkBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(thirdChkBox)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(forthChkBox)
                                .addGap(12, 12, 12))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(updateUniBtn, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel5)
                            .addComponent(seatsTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(20, 20, 20))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void addAddressBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addAddressBtnActionPerformed
        // TODO add your handling code here:
        String address = browser.getSelectedText();
        addressTxtField.setText(address);
    }//GEN-LAST:event_addAddressBtnActionPerformed

    private void addPhNobtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_addPhNobtnActionPerformed
        // TODO add your handling code here:
        String phNo = browser.getSelectedText();
        phNoTxtField.setText(phNo);
    }//GEN-LAST:event_addPhNobtnActionPerformed

    private void emailAddBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_emailAddBtnActionPerformed
        // TODO add your handling code here:
        String email = browser.getSelectedText();
        emailTxtField.setText(email);
    }//GEN-LAST:event_emailAddBtnActionPerformed

    private void updateUniBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_updateUniBtnActionPerformed
        // TODO add your handling code here:
        boolean validInfo=true;
        if(addressTxtField.getText().equals("")||phNoTxtField.getText().equals("")||emailTxtField.getText().equals("")||seatsValid==1
                ||seatsTxtField.getText().equals("")){
            validInfo=false;
        }
        
        if(validInfo){
        u.setAddress(addressTxtField.getText());
        u.setPhNo(phNoTxtField.getText());
        u.setUniEmail(emailTxtField.getText());
        u.setSeats(Integer.parseInt(seatsTxtField.getText()));
        if(firstChkBox.isSelected()){
            u.setS20(true);
        }
        if(secondChkBox.isSelected()){
            u.setS50(true);
        }
        if(thirdChkBox.isSelected()){
            u.setS70(true);
        }
        if(forthChkBox.isSelected()){
            u.setS100(true);
        }
        u.setStatus("Updated");
        main.login1();
        JOptionPane.showMessageDialog(null, "University updated and uploaded Successfully");
        financialAid.logger.info("University updated and uploaded Successfully" + u.getUniName());
//        dB4OUni.storeSystem(u);
        
        UniProfileUpdated aPUP = new UniProfileUpdated(CardSequenceJPanel);

        CardSequenceJPanel.add("UniProfileUpdated",aPUP);
        CardLayout layout = (CardLayout) CardSequenceJPanel.getLayout();
        layout.next(CardSequenceJPanel);
        
        }else{
           JOptionPane.showMessageDialog(null, "Please enter valid info"); 
        }
    }//GEN-LAST:event_updateUniBtnActionPerformed

    private void seatsTxtFieldKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_seatsTxtFieldKeyReleased
        // TODO add your handling code here:
        try{

            int valid = Integer.parseInt(seatsTxtField.getText());
            seatsValid=0;

        }catch(NumberFormatException e){

            seatsValid=1;

        }
    }//GEN-LAST:event_seatsTxtFieldKeyReleased


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton addAddressBtn;
    private javax.swing.JButton addPhNobtn;
    private javax.swing.JTextField addressTxtField;
    private javax.swing.JButton emailAddBtn;
    private javax.swing.JTextField emailTxtField;
    private javax.swing.JCheckBox firstChkBox;
    private javax.swing.JCheckBox forthChkBox;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel map;
    private javax.swing.JTextField phNoTxtField;
    private javax.swing.JTextField searchTxtField;
    private javax.swing.JTextField seatsTxtField;
    private javax.swing.JCheckBox secondChkBox;
    private javax.swing.JCheckBox thirdChkBox;
    private javax.swing.JButton updateUniBtn;
    // End of variables declaration//GEN-END:variables
}
