/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Interface.Student;

import Business.AllStudents;
import Business.AllUniversity;
import Business.Student;
import Business.University;
import Interface.ImageLoader;
import com.teamdev.jxbrowser.chromium.Browser;
import com.teamdev.jxbrowser.chromium.swing.BrowserView;
import java.awt.Color;
import java.awt.Image;
import java.util.Date;
import java.util.Properties;
import javax.mail.Message;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

/**
 *
 * @author aayush
 */
public class StudentData extends javax.swing.JPanel {

    /**
     * Creates new form StudentData
     */
    private JPanel CardSequenceJPanel;
    private AllStudents allStudents;
    private AllUniversity allUniversity;
    final Browser browser = new Browser();
    BrowserView view = new BrowserView(browser);
    public StudentData(JPanel CardSequenceJPanel,AllStudents allStudents,AllUniversity allUniversity) {
        initComponents();
        this.CardSequenceJPanel=CardSequenceJPanel;
        this.allStudents=allStudents;
        this.allUniversity=allUniversity;
        amountTxtField.setEnabled(false);
        uni1ComboBox.setEnabled(false);
        uni2ComboBox.setEnabled(false);
        seatsTxtField.setEditable(false);
        btn20.setEnabled(false);
        btn50.setEnabled(false);
        btn70.setEnabled(false);
        btn100.setEnabled(false);
        //addComboBoxItem();
        loadMap();
    }
    
    private void comboboxCheck(){
        if(!pSpon.isSelected()&& !uScho.isSelected()){
            amountTxtField.setEnabled(false);
            uni1ComboBox.setEnabled(false);
            uni2ComboBox.setEnabled(false);
            btn20.setEnabled(false);
            btn50.setEnabled(false);
            btn70.setEnabled(false);
            btn100.setEnabled(false);
            uniLbl.setText("University:");
        }
    }
    
    private void addComboBoxItem(){
//        uni1ComboBox.removeAllItems();
//        uni2ComboBox.removeAllItems();
        if(uni1ComboBox.getSelectedItem() != null){
                    uni1ComboBox.removeAllItems();
                    
        }
        if(uni2ComboBox.getSelectedItem() !=null){
                    uni2ComboBox.removeAllItems();
        }
        
        for(University u: allUniversity.getAllUniversity()){
            
            if(uScho.isSelected()){
                if(u.getStatus().equals("Updated") && (u.isS100() || u.isS70()
                            || u.isS50() || u.isS20()) && (u.getSeats()>0)){
                    
                    uni1ComboBox.addItem(u.getUniName());
                    uni2ComboBox.addItem(u.getUniName());
                }
                
            }else if(pSpon.isSelected()){
                    if(u.getStatus().equals("Updated") && !u.isS100() && !u.isS70()
                            && !u.isS50() && !u.isS20()&& (u.getSeats()>0)){
                        uni1ComboBox.addItem(u.getUniName());
                    }
                }
        }
    }
    
    private void loadMap(){
//        String selected =(String) uni1ComboBox.getSelectedItem();
//       
//       String s=selected.replace(" ", "+");
       browser.loadURL("http://www.google.com/maps");
       map.add(view);
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        buttonGroup2 = new javax.swing.ButtonGroup();
        buttonGroup3 = new javax.swing.ButtonGroup();
        buttonGroup4 = new javax.swing.ButtonGroup();
        buttonGroup5 = new javax.swing.ButtonGroup();
        buttonGroup6 = new javax.swing.ButtonGroup();
        buttonGroup7 = new javax.swing.ButtonGroup();
        jRadioButton1 = new javax.swing.JRadioButton();
        buttonGroup8 = new javax.swing.ButtonGroup();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        nameTxtField = new javax.swing.JTextField();
        amountTxtField = new javax.swing.JTextField();
        submitBtn = new javax.swing.JButton();
        chosenImg = new javax.swing.JLabel();
        avatar1 = new javax.swing.JButton();
        avatar2 = new javax.swing.JButton();
        avatar3 = new javax.swing.JButton();
        avatar4 = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        emailIdTextField = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        sUNameTxtField = new javax.swing.JTextField();
        sPassTxtField = new javax.swing.JTextField();
        map = new javax.swing.JPanel();
        uScho = new javax.swing.JRadioButton();
        pSpon = new javax.swing.JRadioButton();
        uniLbl = new javax.swing.JLabel();
        uni1ComboBox = new javax.swing.JComboBox();
        jLabel7 = new javax.swing.JLabel();
        uni2ComboBox = new javax.swing.JComboBox();
        btn20 = new javax.swing.JButton();
        btn50 = new javax.swing.JButton();
        btn70 = new javax.swing.JButton();
        btn100 = new javax.swing.JButton();
        seatsTxtField = new javax.swing.JTextField();

        jRadioButton1.setText("jRadioButton1");

        jLabel1.setText("Name:");

        jLabel2.setText("University 2:");

        amountTxtField.setText("Amount");
        amountTxtField.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                amountTxtFieldMouseClicked(evt);
            }
        });

        submitBtn.setText("Next Page");
        submitBtn.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                submitBtnActionPerformed(evt);
            }
        });

        chosenImg.setText("Choose Avatar");

        avatar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/boy.png"))); // NOI18N
        avatar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                avatar1ActionPerformed(evt);
            }
        });

        avatar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/boy2.png"))); // NOI18N
        avatar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                avatar2ActionPerformed(evt);
            }
        });

        avatar3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/girl.png"))); // NOI18N
        avatar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                avatar3ActionPerformed(evt);
            }
        });

        avatar4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Images/girl2.png"))); // NOI18N
        avatar4.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                avatar4ActionPerformed(evt);
            }
        });

        jLabel3.setText("Email id:");

        jLabel4.setText("UserName:");

        jLabel5.setText("Password:");

        map.setBackground(new java.awt.Color(255, 255, 255));
        map.setLayout(new java.awt.BorderLayout());

        uScho.setText("University Schorship");
        uScho.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                uSchoMouseClicked(evt);
            }
        });
        uScho.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uSchoActionPerformed(evt);
            }
        });

        pSpon.setText("Public Sponsorship");
        pSpon.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                pSponMouseClicked(evt);
            }
        });
        pSpon.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pSponActionPerformed(evt);
            }
        });

        uniLbl.setText("University: 1");

        uni1ComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uni1ComboBoxActionPerformed(evt);
            }
        });

        jLabel7.setText("Sponsorships available:");

        uni2ComboBox.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                uni2ComboBoxActionPerformed(evt);
            }
        });

        btn20.setFont(new java.awt.Font("Lucida Grande", 0, 8)); // NOI18N
        btn20.setText("20%");

        btn50.setFont(new java.awt.Font("Lucida Grande", 0, 8)); // NOI18N
        btn50.setText("50%");

        btn70.setFont(new java.awt.Font("Lucida Grande", 0, 8)); // NOI18N
        btn70.setText("70%");

        btn100.setFont(new java.awt.Font("Lucida Grande", 0, 8)); // NOI18N
        btn100.setText("100%");

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(this);
        this.setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(map, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addGap(15, 15, 15)
                .addComponent(chosenImg, javax.swing.GroupLayout.PREFERRED_SIZE, 125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel3)
                            .addComponent(jLabel1))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(nameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(emailIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, 151, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(0, 0, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel5)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(sPassTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 153, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel4)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(sUNameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 148, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 6, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 97, Short.MAX_VALUE)))
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(jLabel2)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(uni2ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                            .addComponent(uniLbl)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(uni1ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, 154, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(pSpon)
                            .addComponent(uScho))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(seatsTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(amountTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, 70, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 45, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel7)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                .addGap(6, 6, 6)
                                .addComponent(btn20, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(btn50, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(avatar1, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(avatar2, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(btn70, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(avatar3, javax.swing.GroupLayout.PREFERRED_SIZE, 56, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(avatar4, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btn100, javax.swing.GroupLayout.PREFERRED_SIZE, 51, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(submitBtn)
                .addGap(24, 24, 24))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(map, javax.swing.GroupLayout.DEFAULT_SIZE, 461, Short.MAX_VALUE)
                .addGap(35, 35, 35)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(submitBtn, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(pSpon)
                                        .addComponent(nameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(jLabel1)
                                        .addComponent(amountTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(jLabel3)
                                        .addComponent(emailIdTextField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(uScho)
                                        .addComponent(seatsTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                .addGroup(layout.createSequentialGroup()
                                    .addComponent(jLabel7)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                        .addComponent(btn20, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btn50, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btn70, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addComponent(btn100, javax.swing.GroupLayout.PREFERRED_SIZE, 46, javax.swing.GroupLayout.PREFERRED_SIZE))))
                            .addGap(18, 18, 18)
                            .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(uniLbl)
                                            .addComponent(uni1ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(10, 10, 10)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel2)
                                            .addComponent(uni2ComboBox, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                                    .addGroup(layout.createSequentialGroup()
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel4)
                                            .addComponent(sUNameTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(8, 8, 8)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                            .addComponent(jLabel5)
                                            .addComponent(sPassTxtField, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(avatar1, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(avatar2, javax.swing.GroupLayout.PREFERRED_SIZE, 54, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(avatar4, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(avatar3, javax.swing.GroupLayout.PREFERRED_SIZE, 53, javax.swing.GroupLayout.PREFERRED_SIZE)))
                            .addGap(16, 16, 16))
                        .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                            .addComponent(chosenImg, javax.swing.GroupLayout.PREFERRED_SIZE, 122, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(38, 38, 38)))))
        );
    }// </editor-fold>//GEN-END:initComponents

    private void submitBtnActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_submitBtnActionPerformed
        // TODO add your handling code here:
        //Student student = allStudents.addStudent();
        
        if(pSpon.isSelected()){
        Student student = allStudents.addWaitStudent();
        student.setName(nameTxtField.getText());
        student.setAmount(Double.parseDouble(amountTxtField.getText()));
        student.setImg(chosenImg.getIcon());
        student.setMinvalue(0);
        student.setMyEmailAdd(emailIdTextField.getText());
        student.setuName(sUNameTxtField.getText());
        student.setPass(sPassTxtField.getText());
        String oUni = (String)uni1ComboBox.getSelectedItem();
        student.setOpenUni(oUni);
        student.setStatus("Sent For Admin Approval");
        allStudents.getStudentRecordDir().add(student);
        sendMail(student);
        
        for(University u: allUniversity.getAllUniversity()){
            if(u.getUniName().equals(oUni)){
                int seats=u.getSeats();
                seats--;
                u.setSeats(seats);
            }
        }
        
        JOptionPane.showMessageDialog(null, "Student submitted Successfully for public sponsorship");
        nameTxtField.setText("");
        amountTxtField.setText("");
        emailIdTextField.setText("");
        sUNameTxtField.setText("");
        sPassTxtField.setText("");
        chosenImg.setIcon(null);
        chosenImg.revalidate();
        pSpon.setSelected(false);
        uScho.setSelected(false);
        comboboxCheck();
        
        
        }else if(uScho.isSelected()){
            
        Student student = allStudents.addWaitStudent();
        student.setName(nameTxtField.getText());
        student.setImg(chosenImg.getIcon());
        student.setMinvalue(0);
        student.setMyEmailAdd(emailIdTextField.getText());
        student.setuName(sUNameTxtField.getText());
        student.setPass(sPassTxtField.getText());
        String Uni1 = (String)uni1ComboBox.getSelectedItem();
        String Uni2 = (String)uni2ComboBox.getSelectedItem();
        student.setUni1(Uni1);
        student.setUni2(Uni2);
        student.setStatus("Sent For Admin Approval");
        allStudents.getStudentRecordDir().add(student);
        sendMail(student);
        
        for(University u: allUniversity.getAllUniversity()){
            if(u.getUniName().equals(Uni1)){
                int seats=u.getSeats();
                seats--;
                u.setSeats(seats);
            }
            if(u.getUniName().equals(Uni2)){
                int seats=u.getSeats();
                seats--;
                u.setSeats(seats);
            }
        }
        
        JOptionPane.showMessageDialog(null, "Student submitted Successfully for University Schlorship");
        nameTxtField.setText("");
        //amountTxtField.setText("");
        emailIdTextField.setText("");
        sUNameTxtField.setText("");
        sPassTxtField.setText("");
        chosenImg.setIcon(null);
        chosenImg.revalidate();
        pSpon.setSelected(false);
        uScho.setSelected(false);
        comboboxCheck();
            
        }else{
            JOptionPane.showMessageDialog(null, "Please select type of sponsorship");
        }
        
        
    }//GEN-LAST:event_submitBtnActionPerformed

    public void sendMail(Student s){
        try{
            
            String host ="smtp.gmail.com" ;
            String user = "aayush.rks@gmail.com";
            String pass = "Mumbai2606$";
            String to = s.getMyEmailAdd();
            String from = "aayush.rks@gmail.com";
            String subject = "Welcome to goFundYourself !!";
            String messageText = "Hi "+s.getName()+"\n\nWelcome to goFundYourself, Your profile has been sent for approval."
                    + "\n\nWe are happy to have you."
                    + "\n\n\nRegards,\ngoFundYourself Team\nNEU,Boston\nMA.";
            boolean sessionDebug = false;

            Properties props = System.getProperties();

            props.put("mail.smtp.starttls.enable", "true");
            props.put("mail.smtp.host", host);
            props.put("mail.smtp.port", "587");
            props.put("mail.smtp.auth", "true");
            props.put("mail.smtp.starttls.required", "true");

            java.security.Security.addProvider(new com.sun.net.ssl.internal.ssl.Provider());
            Session mailSession = Session.getDefaultInstance(props, null);
            mailSession.setDebug(sessionDebug);
            Message msg = new MimeMessage(mailSession);
            msg.setFrom(new InternetAddress(from));
            InternetAddress[] address = {new InternetAddress(to)};
            msg.setRecipients(Message.RecipientType.TO, address);
            msg.setSubject(subject); msg.setSentDate(new Date());
            msg.setText(messageText);

           Transport transport=mailSession.getTransport("smtp");
           transport.connect(host, user, pass);
           transport.sendMessage(msg, msg.getAllRecipients());
           transport.close();
           System.out.println("message send successfully");
        }catch(Exception ex)
        {
            System.out.println(ex);
        }

        
    }
    private void avatar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_avatar1ActionPerformed
        // TODO add your handling code here:
        chosenImg.setText("");
        ImageIcon imageIcon = new ImageIcon(ImageLoader.loadImageIcon("Images/boyShow.png").getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        chosenImg.setIcon(imageIcon);
        
    }//GEN-LAST:event_avatar1ActionPerformed

    private void avatar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_avatar2ActionPerformed
        // TODO add your handling code here:
        chosenImg.setText("");
        ImageIcon imageIcon = new ImageIcon(ImageLoader.loadImageIcon("Images/boy2Show.png").getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        chosenImg.setIcon(imageIcon);      
    }//GEN-LAST:event_avatar2ActionPerformed

    private void avatar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_avatar3ActionPerformed
        // TODO add your handling code here:
        chosenImg.setText("");
        ImageIcon imageIcon = new ImageIcon(ImageLoader.loadImageIcon("Images/girlShow.png").getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        chosenImg.setIcon(imageIcon);
        
    }//GEN-LAST:event_avatar3ActionPerformed

    private void avatar4ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_avatar4ActionPerformed
        // TODO add your handling code here:
        chosenImg.setText("");
        ImageIcon imageIcon = new ImageIcon(ImageLoader.loadImageIcon("Images/girl2Show.png").getImage().getScaledInstance(100, 100, Image.SCALE_DEFAULT));
        chosenImg.setIcon(imageIcon);
        
    }//GEN-LAST:event_avatar4ActionPerformed

    private void pSponMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_pSponMouseClicked
        // TODO add your handling code here:
        //do nothing
        
    }//GEN-LAST:event_pSponMouseClicked

    private void uSchoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_uSchoMouseClicked
        // TODO add your handling code here:
        //do nothing
    }//GEN-LAST:event_uSchoMouseClicked

    private void pSponActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pSponActionPerformed
        // TODO add your handling code here:
        uScho.setSelected(false);
        amountTxtField.setEnabled(true);
        uni1ComboBox.setEnabled(true);
        uni2ComboBox.setEnabled(false);
        btn20.setEnabled(false);
        btn20.setForeground(Color.black);
        btn50.setEnabled(false);
        btn50.setForeground(Color.black);
        btn70.setEnabled(false);
        btn70.setForeground(Color.black);
        btn100.setEnabled(false);
        btn100.setForeground(Color.black);
        uniLbl.setText("University:");
        comboboxCheck();
        addComboBoxItem();
    }//GEN-LAST:event_pSponActionPerformed

    private void uSchoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uSchoActionPerformed
        // TODO add your handling code here:
        pSpon.setSelected(false);
        amountTxtField.setEnabled(false);
        uni1ComboBox.setEnabled(true);
        uni2ComboBox.setEnabled(true);
        btn20.setEnabled(true);
        btn50.setEnabled(true);
        btn70.setEnabled(true);
        btn100.setEnabled(true);
        uniLbl.setText("University 1:");
        comboboxCheck();
        addComboBoxItem();
    }//GEN-LAST:event_uSchoActionPerformed

    private void uni1ComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uni1ComboBoxActionPerformed
        // TODO add your handling code here:
       String selected =(String) uni1ComboBox.getSelectedItem();
       if(selected!=null){
       String s=selected.replace(" ", "+");
       browser.loadURL("https://www.google.com/maps/search/?api=1&query="+s);
       map.add(view);
       }
       
       for(University u:allUniversity.getAllUniversity()){
           if(u.getUniName().equals(selected)){
               seatsTxtField.setText(Integer.toString(u.getSeats()));
               if(u.isS20()){
                   btn20.setBackground(Color.green);
                   btn20.setForeground(Color.red);
               }else{
                   btn20.setBackground(Color.gray);
                   btn20.setForeground(Color.black);
               }
               if(u.isS50()){
                   btn50.setBackground(Color.green);
                   btn50.setForeground(Color.red);
               }else{
                   btn50.setBackground(Color.gray);
                   btn50.setForeground(Color.black);
               }
               if(u.isS70()){
                  btn70.setBackground(Color.green);
                   btn70.setForeground(Color.red); 
               }else{
                   btn70.setBackground(Color.gray);
                   btn70.setForeground(Color.black);
               }
               if(u.isS100()){
                   btn100.setBackground(Color.green);
                   btn100.setForeground(Color.red);
               }else{
                   btn100.setBackground(Color.gray);
                   btn100.setForeground(Color.black);
               }
           }
       }
        
    }//GEN-LAST:event_uni1ComboBoxActionPerformed

    private void uni2ComboBoxActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_uni2ComboBoxActionPerformed
        // TODO add your handling code here:
        
        String selected =(String) uni2ComboBox.getSelectedItem();
        if(selected!=null){
        String s=selected.replace(" ", "+");        
        browser.loadURL("https://www.google.com/maps/search/?api=1&query="+s);
        map.add(view);
        }
        
        for(University u:allUniversity.getAllUniversity()){
           if(u.getUniName().equals(selected)){
               seatsTxtField.setText(Integer.toString(u.getSeats()));
               if(u.isS20()){
                   btn20.setBackground(Color.green);
                   btn20.setForeground(Color.red);
               }else{
                   btn20.setBackground(Color.gray);
                   btn20.setForeground(Color.black);
               }
               if(u.isS50()){
                   btn50.setBackground(Color.green);
                   btn50.setForeground(Color.red);
               }else{
                   btn50.setBackground(Color.gray);
                   btn50.setForeground(Color.black);
               }
               if(u.isS70()){
                  btn70.setBackground(Color.green);
                   btn70.setForeground(Color.red); 
               }else{
                   btn70.setBackground(Color.gray);
                   btn70.setForeground(Color.black);
               }
               if(u.isS100()){
                   btn100.setBackground(Color.green);
                   btn100.setForeground(Color.red);
               }else{
                   btn100.setBackground(Color.gray);
                   btn100.setForeground(Color.black);
               }
           }
       }
        
    }//GEN-LAST:event_uni2ComboBoxActionPerformed

    private void amountTxtFieldMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_amountTxtFieldMouseClicked
        // TODO add your handling code here:
        amountTxtField.setText("");
    }//GEN-LAST:event_amountTxtFieldMouseClicked


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextField amountTxtField;
    private javax.swing.JButton avatar1;
    private javax.swing.JButton avatar2;
    private javax.swing.JButton avatar3;
    private javax.swing.JButton avatar4;
    private javax.swing.JButton btn100;
    private javax.swing.JButton btn20;
    private javax.swing.JButton btn50;
    private javax.swing.JButton btn70;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    private javax.swing.ButtonGroup buttonGroup3;
    private javax.swing.ButtonGroup buttonGroup4;
    private javax.swing.ButtonGroup buttonGroup5;
    private javax.swing.ButtonGroup buttonGroup6;
    private javax.swing.ButtonGroup buttonGroup7;
    private javax.swing.ButtonGroup buttonGroup8;
    private javax.swing.JLabel chosenImg;
    private javax.swing.JTextField emailIdTextField;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JRadioButton jRadioButton1;
    private javax.swing.JPanel map;
    private javax.swing.JTextField nameTxtField;
    private javax.swing.JRadioButton pSpon;
    private javax.swing.JTextField sPassTxtField;
    private javax.swing.JTextField sUNameTxtField;
    private javax.swing.JTextField seatsTxtField;
    private javax.swing.JButton submitBtn;
    private javax.swing.JRadioButton uScho;
    private javax.swing.JComboBox uni1ComboBox;
    private javax.swing.JComboBox uni2ComboBox;
    private javax.swing.JLabel uniLbl;
    // End of variables declaration//GEN-END:variables
}
